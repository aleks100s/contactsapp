# ContactsApp
ContactsApp is a test application that downloads employee list from the given urls, merges them and saves in local database
App uses MVC architecture pattern which suits best in such a small app

Supported OS versions: iOS 13.0 and higher

## Project requirements

Project requires XCode 11 or higher. Xcode 11 requires a Mac running macOS Mojave 10.14.4 or later.

## How to run the project

Change bundle identifier and select your team on Signing & Capabilities tab in target settings
Select a simulator or device you want to run on and hit Start button
