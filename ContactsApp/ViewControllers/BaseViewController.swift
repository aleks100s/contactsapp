//
//  BaseViewController.swift
//  ContactsApp
//
//  Created by Admin on 09.11.2021.
//

import UIKit

class BaseViewController: UIViewController {
    func subviews() -> [UIView] {
        []
    }
    
    func createConstraints() {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubviews(self.subviews())
        createConstraints()
    }
    
    func showAlert(title: String, message: String? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        present(alert, animated: true)
    }
}
