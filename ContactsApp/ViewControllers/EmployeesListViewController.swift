//
//  ContactsListViewController.swift
//  ContactsApp
//
//  Created by Admin on 09.11.2021.
//

import UIKit
import CoreData
import Contacts
import ContactsUI

final class EmployeesListViewController: BaseViewController {
    private let contactsManager = ContactsManager()
    private let networkManager = NetworkManager()
    private let databaseManager = DatabaseManager()
    private var searchText: String? = nil {
        didSet {
            databaseManager.fetchEmployeesFromDatabase(with: searchText)
        }
    }

    private var allEmployees: [EmployeeEntity] = []
    private var units: [CompanyUnit] = [] {
        didSet {
            refreshTable()
        }
    }
    private var userContacts: [CNContact] = [] {
        didSet {
            refreshTable()
        }
    }
    private let spinner = UIRefreshControl()
    private lazy var tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.backgroundColor = .clear
        table.dataSource = self
        table.delegate = self
        table.register(ContactItemTableViewCell.self, forCellReuseIdentifier: ContactItemTableViewCell.identifier)
        spinner.addTarget(self, action: #selector(downloadContacts), for: .valueChanged)
        table.addSubview(spinner)
        return table
    }()
    
    override func subviews() -> [UIView] {
        [tableView]
    }
    
    override func createConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSelf()
        setupSearchBar()
        requestContacts()
        databaseManager.delegate = self
        databaseManager.fetchEmployeesFromDatabase(with: searchText)
        downloadContacts()
    }
    
    private func setupSelf() {
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Contacts"
    }
    
    private func setupSearchBar() {
        let search = UISearchController(searchResultsController: nil)
        search.delegate = self
        search.searchBar.delegate = self
        self.navigationItem.searchController = search
    }
    
    private func requestContacts() {
        contactsManager.requestContacts { [weak self] contacts in
            guard let contacts = contacts else {
                return
            }
            self?.userContacts = contacts
        }
    }
    
    @objc private func downloadContacts() {
        networkManager.loadEmployees { [weak self] employees, error in
            DispatchQueue.main.async {
                self?.spinner.endRefreshing()
                if let error = error {
                    self?.showError(error)
                }
                self?.databaseManager.updateEmployeesData(employees)
                self?.searchText = nil
            }
        }
    }
    
    private func getMatchingContactIfExists(for employee: Employee) -> CNContact? {
        userContacts.first { contact in
            contact.givenName == employee.firstName && contact.familyName == employee.lastName
        }
    }
    
    private func openDetailController(employee: Employee, contact: CNContact?) {
        let controller = EmployeeDetailViewController()
        controller.employee = employee
        controller.contact = contact
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func openContact(_ contact: CNContact) {
        let contactViewController = CNContactViewController(forUnknownContact: contact)
        contactViewController.hidesBottomBarWhenPushed = true
        contactViewController.allowsEditing = false
        contactViewController.allowsActions = false
        navigationController?.pushViewController(contactViewController, animated: true)
    }
    
    private func refreshTable() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
        
    private func showError(_ error: Error) {
        showAlert(title: "Error occurred", message: error.localizedDescription)
    }
}

extension EmployeesListViewController: DatabaseManagerDelegate {
    func databaseManager(_: DatabaseManager?, gotError error: Error) {
        showError(error)
    }
    
    func databaseManager(_: DatabaseManager, didFetchData data: [CompanyUnit]?) {
        if let units = data {
            self.units = units
        }
    }
}

extension EmployeesListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        units.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        units[section].position.rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return units[section].employees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ContactItemTableViewCell.identifier, for: indexPath) as? ContactItemTableViewCell, indexPath.row < units[indexPath.section].employees.count else {
            return UITableViewCell()
        }
        let employee = units[indexPath.section].employees[indexPath.row]
        cell.displayEmployee(employee)
        if let contact = getMatchingContactIfExists(for: employee) {
            cell.bindContact(contact) { [weak self] in
                self?.openContact(contact)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let employee = units[indexPath.section].employees[indexPath.row]
        let contact = getMatchingContactIfExists(for: employee)
        openDetailController(employee: units[indexPath.section].employees[indexPath.row], contact: contact)
    }
}

extension EmployeesListViewController: UISearchControllerDelegate, UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchText = nil
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            self.searchText = nil
            return
        }
        self.searchText = searchText
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        navigationItem.searchController?.dismiss(animated: true, completion: nil)
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        self.searchText = nil
    }
}
