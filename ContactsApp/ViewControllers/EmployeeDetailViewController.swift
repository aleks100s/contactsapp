//
//  ContactDetailViewController.swift
//  ContactsApp
//
//  Created by Admin on 09.11.2021.
//

import UIKit
import Contacts
import ContactsUI

final class EmployeeDetailViewController: BaseViewController {
    var employee: Employee?
    var contact: CNContact?
    private lazy var scrollView = UIScrollView(contentView: contentView, insets: UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16), showsScrollIndicator: false)
    private lazy var contentView = UIStackView(axis: .vertical, distribution: .fill, spacing: 16)
    private lazy var tableView: UITableView = {
        let table = AutosizeTableView()
        table.bounces = false
        table.backgroundColor = .clear
        table.separatorStyle = .none
        table.allowsSelection = false
        table.dataSource = self
        return table
    }()
        
    override func subviews() -> [UIView] {
        [scrollView]
    }
    
    override func createConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = employee?.fullName
        if contact != nil {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "View in Contacts", style: .plain, target: self, action: #selector(showUserContact))
        }
        setupContent()
    }
        
    @objc private func showUserContact() {
        if let contact = contact {
            let contactViewController = CNContactViewController(forUnknownContact: contact)
            contactViewController.allowsEditing = false
            contactViewController.allowsActions = false
            navigationController?.pushViewController(contactViewController, animated: true)
        }
    }
    
    private func setupContent() {
        if let phone = employee?.contactDetails?.phone {
            contentView.addArrangedSubview(EmployeeAdditionView(description: "Phone", mainValue: phone))
        }
        if let email = employee?.contactDetails?.email {
            contentView.addArrangedSubview(EmployeeAdditionView(description: "E-Mail", mainValue: email))
        }
        contentView.addArrangedSubview(EmployeeAdditionView(description: "Position", mainValue: employee?.position.rawValue ?? ""))

        if !(employee?.projects.isEmpty ?? true) {
            contentView.addArrangedSubview(tableView)
        }
    }
}

extension EmployeeDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        employee?.projects.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = employee?.projects[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Projects"
    }
}
