//
//  ContactView.swift
//  ContactsApp
//
//  Created by Admin on 09.11.2021.
//

import UIKit

final class EmployeeAdditionView: UIView {
    private let descriptionLabel = UILabel(font: .systemFont(ofSize: 15, weight: .regular), color: .secondaryLabel)
    private let mainLabel = UILabel(font: .systemFont(ofSize: 17, weight: .medium), color: .label)
    
    init(description: String, mainValue: String) {
        super.init(frame: .zero)
        descriptionLabel.text = description
        mainLabel.text = mainValue
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        layer.masksToBounds = true
        layer.cornerRadius = 8
        layer.backgroundColor = UIColor.systemGray.withAlphaComponent(0.5).cgColor
        addSubviews([mainLabel, descriptionLabel])
        mainLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 12),
            descriptionLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
            descriptionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
            mainLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 8),
            mainLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
            mainLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12),
            mainLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
        ])
    }
}
