//
//  NativeContactButton.swift
//  ContactsApp
//
//  Created by Admin on 09.11.2021.
//

import UIKit

final class NativeContactButton: UIButton {
    init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        if #available(iOS 15.0, *) {
            var configuration = UIButton.Configuration.plain()
            configuration.baseBackgroundColor = .clear
            configuration.contentInsets = NSDirectionalEdgeInsets(top: 6, leading: 6, bottom: 6, trailing: 6)
            var containter = AttributeContainer()
            containter.font = .systemFont(ofSize: 13, weight: .regular)
            containter.foregroundColor = .systemBlue
            configuration.attributedTitle = AttributedString("View in Contacts", attributes: containter)
            self.configuration = configuration
        } else {
            backgroundColor = .clear
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
            titleLabel?.font = .systemFont(ofSize: 13, weight: .regular)
            setTitle("View in Contacts", for: .normal)
            setTitleColor(.systemBlue, for: .normal)
        }
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = UIColor.systemBlue.cgColor
    }
    
    override var intrinsicContentSize: CGSize {
        var size = super.intrinsicContentSize
        if #available(iOS 15.0, *) {
            size.width += ((configuration?.contentInsets.leading ?? 0) + (configuration?.contentInsets.trailing ?? 0))
        } else {
            size.width += (titleEdgeInsets.left + titleEdgeInsets.right)
        }
        return size
    }
}
