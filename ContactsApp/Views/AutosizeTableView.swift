//
//  AutosizeTableView.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//

import UIKit

/// automatically adjusts height to contentSize
final class AutosizeTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height + contentInset.bottom + contentInset.top)
    }
}
