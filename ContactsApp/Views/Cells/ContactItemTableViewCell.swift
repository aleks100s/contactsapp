//
//  ContactItemTableViewCell.swift
//  ContactsApp
//
//  Created by Admin on 09.11.2021.
//

import UIKit
import Contacts

final class ContactItemTableViewCell: UITableViewCell {
    static var identifier: String {
        String(describing: Self.self)
    }
    
    private let contactFullNameLabel = UILabel(font: .systemFont(ofSize: 17, weight: .medium), color: .label)
    private let nativeContactButton = NativeContactButton()
    private var contactPressedHandler: (() -> ())? = nil
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        nativeContactButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        backgroundColor = .systemBackground
        contentView.addSubviews([contactFullNameLabel, nativeContactButton])
        contactFullNameLabel.translatesAutoresizingMaskIntoConstraints = false
        nativeContactButton.translatesAutoresizingMaskIntoConstraints = false
        nativeContactButton.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        nativeContactButton.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        contactFullNameLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        contactFullNameLabel.numberOfLines = 0
        NSLayoutConstraint.activate([
            contactFullNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            contactFullNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            contactFullNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12),
            nativeContactButton.centerYAnchor.constraint(equalTo: contactFullNameLabel.centerYAnchor),
            nativeContactButton.leadingAnchor.constraint(equalTo: contactFullNameLabel.trailingAnchor, constant: 10),
            nativeContactButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        ])
    }
    
    @objc func buttonPressed() {
        contactPressedHandler?()
    }
    
    func displayEmployee(_ employee: Employee, contact: CNContact? = nil, handler: (() -> ())? = nil) {
        contactFullNameLabel.text = "\(employee.firstName ?? "") \(employee.lastName ?? "")"
        nativeContactButton.isHidden = true
    }
    
    func bindContact(_ contact: CNContact? = nil, handler: (() -> ())? = nil) {
        nativeContactButton.isHidden = contact == nil
        self.contactPressedHandler = handler
    }
}
