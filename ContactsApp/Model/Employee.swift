//
//  Contact.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//

import Foundation

struct Employee: Decodable {
    let firstName: String?
    let lastName: String?
    let contactDetails: ContactDetails?
    let position: Position
    let projects: [String]
    
    var fullName: String {
        "\(firstName ?? "") \(lastName ?? "")"
    }
    
    init(entity: EmployeeEntity) {
        self.init(firstName: entity.firstName,
                  lastName: entity.lastName,
                  contactDetails: ContactDetails(email: entity.email,
                                                 phone: entity.phone),
                  position: Position(rawValue: entity.position ?? "") ?? .other,
                  projects: entity.projects?.split(separator: ";").map { String($0) } ?? [])
    }
    
    init(firstName: String? = nil, lastName: String? = nil, contactDetails: Employee.ContactDetails? = nil, position: Position, projects: [String]) {
        self.firstName = firstName
        self.lastName = lastName
        self.contactDetails = contactDetails
        self.position = position
        self.projects = projects
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        firstName = try? container.decode(String.self, forKey: .firstName)
        lastName = try? container.decode(String.self, forKey: .lastName)
        contactDetails = try? container.decode(ContactDetails.self, forKey: .contactDetails)
        let positionString = try? container.decode(String.self, forKey: .position)
        self.position = Position(rawValue: positionString ?? "") ?? .other
        projects = (try? container.decode([String].self, forKey: .projects)) ?? []
    }
    
    private enum CodingKeys: String, CodingKey {
        case firstName = "fname"
        case lastName = "lname"
        case contactDetails = "contact_details"
        case position = "position"
        case projects = "projects"
    }

    final class ContactDetails: Decodable {
        var email: String?
        var phone: String?
        
        internal init(email: String? = nil, phone: String? = nil) {
            self.email = email
            self.phone = phone
        }
        
        public required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            email = try? container.decode(String.self, forKey: .email)
            phone = try? container.decode(String.self, forKey: .phone)
        }
        
        private enum CodingKeys: String, CodingKey {
            case email = "email"
            case phone = "phone"
        }
    }
}
