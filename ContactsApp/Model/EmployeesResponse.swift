//
//  ContactsResponse.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//

import Foundation

final class EmployeesResponse: Decodable {
    var employees: [Employee]
}
