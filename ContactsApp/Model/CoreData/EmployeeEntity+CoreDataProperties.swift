//
//  EmployeeEntity+CoreDataProperties.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//
//

import Foundation
import CoreData

extension EmployeeEntity {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<EmployeeEntity> {
        return NSFetchRequest<EmployeeEntity>(entityName: "EmployeeEntity")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var phone: String?
    @NSManaged public var email: String?
    @NSManaged public var position: String?
    @NSManaged public var projects: String?

}

extension EmployeeEntity : Identifiable {

}
