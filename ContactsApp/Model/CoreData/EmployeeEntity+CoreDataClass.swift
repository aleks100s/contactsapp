//
//  EmployeeEntity+CoreDataClass.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//
//

import Foundation
import CoreData

@objc(EmployeeEntity)
public class EmployeeEntity: NSManagedObject {
    func setupWithRemote(_ employee: Employee) {
        firstName = employee.firstName
        lastName = employee.lastName
        phone = employee.contactDetails?.phone
        email = employee.contactDetails?.email
        position = employee.position.rawValue
        projects = employee.projects.joined(separator: ";")
    }
}
