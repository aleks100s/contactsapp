//
//  Position.swift
//  ContactsApp
//
//  Created by Admin on 12.11.2021.
//

import Foundation

enum Position: String, CaseIterable {
    case iOS = "IOS"
    case android = "ANDROID"
    case web = "WEB"
    case projectManager = "PM"
    case tester = "TESTER"
    case sales = "SALES"
    case other = "OTHER"
}
