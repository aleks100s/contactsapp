//
//  CompanyUnit.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//

import Foundation

struct CompanyUnit {
    let position: Position
    let employees: [Employee]
}
