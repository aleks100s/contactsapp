//
//  ContactsManager.swift
//  ContactsApp
//
//  Created by Admin on 12.11.2021.
//

import Contacts
import ContactsUI

final class ContactsManager {
    private let contactStore = CNContactStore()
    
    func requestContacts(completion: @escaping (([CNContact]?) -> ())) {
        contactStore.requestAccess(for: .contacts) { [weak self] (accessGranted, error) in
            if accessGranted {
                let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactViewController.descriptorForRequiredKeys()]
                let request = CNContactFetchRequest(keysToFetch: keys)
                do {
                    var contacts = [CNContact]()
                    try self?.contactStore.enumerateContacts(with: request) {
                        (contact, stop) in
                        contacts.append(contact)
                    }
                    completion(contacts)
                }
                catch {
                    completion(nil)
                }
            }
        }

    }
}
