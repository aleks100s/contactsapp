//
//  DatabaseManager.swift
//  ContactsApp
//
//  Created by Admin on 12.11.2021.
//

import CoreData

protocol DatabaseManagerDelegate: AnyObject {
    func databaseManager(_ manager: DatabaseManager?, gotError error: Error)
    func databaseManager(_ manager: DatabaseManager, didFetchData data: [CompanyUnit]?)
}

final class DatabaseManager {
    private let container = NSPersistentContainer(name: "ContactsApp")
    weak var delegate: DatabaseManagerDelegate?
    
    init() {
        container.loadPersistentStores { [weak self] storeDescription, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            } else {
                self?.container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
            }
        }
    }
    
    func fetchEmployeesFromDatabase(with query: String?) {
        let request: NSFetchRequest<EmployeeEntity> = EmployeeEntity.fetchRequest()
        if query != nil && !query!.isEmpty {
            let firstNamePredicate = NSPredicate(format: "firstName CONTAINS[c] %@", query!)
            let lastNamePredicate = NSPredicate(format: "lastName CONTAINS[c] %@", query!)
            let emailPredicate = NSPredicate(format: "email CONTAINS[c] %@", query!)
            let projectPredicate = NSPredicate(format: "projects CONTAINS[c] %@", query!)
            let positionPredicate = NSPredicate(format: "position CONTAINS[c] %@", query!)
            request.predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [firstNamePredicate, lastNamePredicate, emailPredicate, projectPredicate, positionPredicate])
        }
        do {
            let entities = try container.viewContext.fetch(request)
            let employees = entities.map { entity in
                Employee(entity: entity)
            }
            groupEmployeesByPosition(employees)
        } catch {
            delegate?.databaseManager(self, gotError: error)
        }
    }
    
    private func groupEmployeesByPosition(_ employees: [Employee]) {
        let groupedEmployees = Dictionary(grouping: employees) { emp in
            emp.position
        }
        let units = groupedEmployees.keys.sorted { p1, p2 in
            p1.rawValue < p2.rawValue
        }.map { position -> CompanyUnit in
            let sortedEmployees = groupedEmployees[position]?.sorted { e1, e2 in
                e1.lastName ?? "" < e2.lastName ?? ""
            } ?? []
            return CompanyUnit(position: position, employees: sortedEmployees)
        }
        delegate?.databaseManager(self, didFetchData: units)
    }
    
    func updateEmployeesData(_ employees: [Employee]) {
        deleteOldData()
        saveNewData(employees)
    }
    
    private func deleteOldData() {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult>
        fetchRequest = NSFetchRequest(entityName: "EmployeeEntity")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        deleteRequest.resultType = .resultTypeObjectIDs
        do {
            let batchDelete = try container.viewContext.execute(deleteRequest) as? NSBatchDeleteResult
            guard let deleteResult = batchDelete?.result as? [NSManagedObjectID] else { return }
            let deletedObjects: [AnyHashable: Any] = [NSDeletedObjectsKey: deleteResult]
            NSManagedObjectContext.mergeChanges(fromRemoteContextSave: deletedObjects, into: [container.viewContext])
        } catch {
            delegate?.databaseManager(self, gotError: error)
        }
    }
    
    private func saveNewData(_ employees: [Employee]) {
        let _ = employees.map { employee -> EmployeeEntity in
            let entity = EmployeeEntity(context: container.viewContext)
            entity.setupWithRemote(employee)
            return entity
        }
        if container.viewContext.hasChanges {
            do {
                try container.viewContext.save()
            } catch {
                delegate?.databaseManager(self, gotError: error)
            }
        }
    }
}
