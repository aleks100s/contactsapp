//
//  NetworkManager.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//

import Foundation

final class NetworkManager {
    private let urlString1 = "https://tartu-jobapp.aw.ee/employee_list/"
    private let urlString2 = "https://tallinn-jobapp.aw.ee/employee_list/"
    
    func loadEmployees(completion: @escaping ([Employee], Error?) -> Void) {
        var urls = [urlString1, urlString2]
        var allEmployees = [Employee]()
        var lastError: Error? = nil
        func startNext() {
            guard let urlString = urls.first else {
                completion(allEmployees, lastError)
                return
            }
            urls.removeFirst()
            loadJson(fromURLString: urlString) { (result) in
                switch result {
                case .success(let data):
                    let (array, error) = self.parse(jsonData: data)
                    if let error = error {
                        lastError = error
                    }
                    if let array = array {
                        allEmployees.append(contentsOf: array)
                    }
                case .failure(let error):
                    lastError = error
                }
                startNext()
            }
        }
        startNext()
    }
    
    private func loadJson(fromURLString urlString: String,
                          completion: @escaping (Result<Data, Error>) -> Void) {
        if let url = URL(string: urlString) {
            let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                }
                if let data = data {
                    completion(.success(data))
                }
            }
            urlSession.resume()
        }
    }
    
    private func parse(jsonData: Data) -> ([Employee]?, Error?) {
        do {
            let response = try JSONDecoder().decode(EmployeesResponse.self, from: jsonData)
            return (response.employees, nil)
        } catch {
            return (nil, error)
        }
    }
}
