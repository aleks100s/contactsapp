//
//  UILabel+Extensions.swift
//  ContactsApp
//
//  Created by Admin on 09.11.2021.
//

import UIKit

extension UILabel {
    convenience init(_ text: String? = nil, font: UIFont, color: UIColor) {
        self.init(frame: .zero)
        self.text = text
        self.font = font
        self.textColor = color
    }
}
