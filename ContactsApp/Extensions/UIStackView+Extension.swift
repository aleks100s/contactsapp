//
//  UIStackView+Extension.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(_ views: [UIView]) {
        for view in views {
            addArrangedSubview(view)
        }
    }

    convenience init(axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution = .fill, spacing: CGFloat = 0, alignment: UIStackView.Alignment = .fill, arrangedSubviews: [UIView] = []) {
        self.init()
        self.axis = axis
        self.distribution = distribution
        self.spacing = spacing
        self.alignment = alignment
        addArrangedSubviews(arrangedSubviews)
    }
}
