//
//  UIView+Extensions.swift
//  ContactsApp
//
//  Created by Admin on 09.11.2021.
//

import UIKit

extension UIView {
    convenience init(subviews: [UIView]) {
        self.init(frame: .zero)
        addSubviews(subviews)
    }
    
    func addSubviews(_ subviews: [UIView]) {
        subviews.forEach { view in
            self.addSubview(view)
        }
    }
}
