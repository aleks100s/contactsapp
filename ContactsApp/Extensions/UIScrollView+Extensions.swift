//
//  UIScrollView+Extensions.swift
//  ContactsApp
//
//  Created by Admin on 10.11.2021.
//

import UIKit

extension UIScrollView {
    convenience init(contentView: UIView, insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), showsScrollIndicator: Bool = true) {
        self.init()
        addSubview(contentView)
        contentInset = insets
        showsVerticalScrollIndicator = showsScrollIndicator
        showsHorizontalScrollIndicator = showsScrollIndicator
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            contentView.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -(contentInset.left + contentInset.right))
        ])
    }
}
